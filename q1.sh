#!/bin/bash
read -p "Please, enter a number: " number

if ((number % 5 == 0)) && ((number % 7 == 0)); then
    echo "Hello World"
elif ((number % 5 == 0)); then
    echo "Hello"
elif ((number % 7 == 0)); then
    echo "World"
else
    echo "$number didn't match criteria."
fi
