# Assignment

Here is the description of the assignment's questions/requirements and the job done for accomplishing the tasks.

## Q1
>If a number is divisible by 5, then print Hello.

>If a number is divisible by 7, then print World.

>If a number is divisible by 5 and 7, then print Hello World

For this requirement I've done the very same thing with both Python and BASH.

These are the files:

- [Python](./q1.py)

- [BASH](./q1.sh)

### How to execute the code

#### `Python`

1. For executing the .py file, [Python](https://www.python.org) needs to be installed. To check if it's installed, open a terminal and execute this:
    ```bash
    python --version
    ``` 
2. Once it's installed, clone the repository or download the file.
3. Open a terminal in the file's location and execute this command:
    ```bash
    python q1.py
    ``` 

#### `BASH`

1. For executing the .sh file, open a bash terminal in the file's location and execute the following commands:

    For setting execute permissions:
    ```bash
    chmod +x ./q1.sh
    ``` 
    For actually executing the file:
    ```bash
    ./q1.sh
    ``` 

## Q2

>Please draw an architecture where through VPC you are configuring Angular and Java instances along with an RDS and using S3 bucket for the storage.

For this, I exported an image from my diagram done in [draw.io](draw.io).

![Image](./q2.png "Diagram")