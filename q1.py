number = int(input("Please, enter a number: "))
if number % 5 == 0 and number % 7 == 0:
    print("Hello World")
elif number % 5 == 0:
    print("Hello")
elif number % 7 == 0:
    print("World")
else:
    print(number, "didn't match criteria.")
